import Load from '@/components/load/Load.vue'
import { mapActions, mapGetters, mapMutations } from 'vuex'

export default {
  name: 'List',
  components: {
    Load
  },
  methods: {
    ...mapActions([
      "search"
    ]),
    ...mapMutations([
      "setModal",
      "selectPokemon"
    ]),
    detail(pokemon) {
      this.setModal(true)
      return new Promise((result) => {
        try {
          if (!pokemon || !pokemon.url) result({})
          else
            fetch(pokemon.url)
            .then(requestPokemon => requestPokemon.json())
            .then(requestPokemon => {
              requestPokemon.img = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/${+ pokemon.id}.gif`
              this.selectPokemon(requestPokemon)
            })
            .finally(() => {
              result({})
            })
        } catch (error) {
          result({})
          console.log(error)
          alert('Oops. i have a problem finding the element')
        }
      })
    },
    changeIMG(id) {
      let el = document.querySelector(`#pk_${id}`),
        firstLinkPNG = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${+id}.png`,
        secondLinkPNG = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${+id}.png`,
        failLoad = `https://www.clipartmax.com/png/full/129-1298464_open-pokeball-download-open-pokeball.png`

      if (el.src != firstLinkPNG && el.src != secondLinkPNG && el.src != failLoad) {
        el.title = 'Sprite SVG not found, load PNG'
        el.alt = 'SVG not found image'
        el.src = firstLinkPNG
      } else if (el.src == firstLinkPNG && el.src != failLoad) {
        el.title = 'Sprite SVG not found, load PNG'
        el.alt = 'SVG not found image'
        el.src = secondLinkPNG
      } else if (el.src == secondLinkPNG) {
        el.title = 'Sprite not found'
        el.alt = 'Not found image'
        el.src = failLoad
      }
    },
    padId(id) {
      id = id.toString()
      return id && "00000".replace(RegExp(".".repeat(id.length) + '$'), id)
    }
  },
  computed: {
    ...mapGetters(["list_page_pokemon"]),
    padPokemons() {
      return this.list_page_pokemon && this.list_page_pokemon.map ? this.list_page_pokemon.map((pokemon) => ({
        id: this.padId(pokemon.id),
        name: pokemon.name.replace(/[_-]/g, ' '),
        img: pokemon.img,
        url: pokemon.url
      })) : []
    }
  },
  mounted() {
    this.search()
  }
}
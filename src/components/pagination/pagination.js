import SlidingPagination from 'vue-sliding-pagination'
import { mapActions, mapGetters, mapMutations } from 'vuex'

export default {
  name: 'Pagination',
  components: {
    SlidingPagination,
  },
  data() {
    return {
      page_size: document.body.offsetWidth,
    }
  },
  computed: {
    ...mapGetters([
      "qtdView",
      "local_list_page_pokemon",
      "totalPages",
      "currentPage"
    ])
  },
  methods: {
    ...mapActions([
      "search"
    ]),
    ...mapMutations([
      "setPagePokemon",
      "setNewToast",
      "setCurrentPage",
      "setStart"
    ]),
    selectInnerPage(page) {
      let screen_items = this.qtdView,
        number_pages_local = this.local_list_page_pokemon,
        list_pokemon = number_pages_local
        .slice(screen_items * (page - 1),
          screen_items * page)

      this.setPagePokemon(list_pokemon)
    },

    pageChangeHandler(to_page) {
      let screen_items = this.qtdView,
        number_pages_local = this.local_list_page_pokemon.length,
        number_pages = this.totalPages

      if (to_page < 1 || to_page > number_pages)
        this.setNewToast('Paging limit')
      else if (to_page != this.currentPage) {
        this.setCurrentPage(to_page)

        if (number_pages_local > number_pages) {
          this.selectInnerPage(to_page)
        } else {
          this.setStart(screen_items * (to_page - 1))
          this.search()
        }
      } else {
        this.setNewToast('Current page')
      }
    },
  },
  mounted() {
    window.onresize = () => {
      this.page_size = document.body.offsetWidth
    }
  },
}
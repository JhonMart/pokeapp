
import { mapActions, mapMutations } from 'vuex'

export default {
  name: 'Search',
  data() {
    return {
      keyword: '',
      types: [],
      abilities: [],
    }
  },
  computed: {
    searchURL() {
      const keyword = this.keyword.trim().toLowerCase().replace(/ /g, '-')
      if (keyword.length) {
        if (isNaN(+keyword)) {
          let type = this.types.filter(ty => ty.name === keyword),
            ability = this.abilities.filter(ab => ab.name === keyword)

          return type.length ? type[0].url :
            ability.length ? ability[0].url :
            'https://pokeapi.co/api/v2/pokemon/' + keyword
        } else {
          return 'https://pokeapi.co/api/v2/pokemon/' + (+this.keyword.trim())
        }
      } else {
        return 'https://pokeapi.co/api/v2/pokemon/'
      }
    }
  },
  methods: {
    ...mapActions([
      "search"
    ]),
    ...mapMutations([
      "setTypeSearch",
      "setNewToast",
      "setStart"
    ]),
    getSearch() {
      return new Promise((resolver) => {
        this.setStart(0)
        this.setTypeSearch(this.searchURL)
        this.search()

        resolver({})
      })
    },

    async startVars() {
      await fetch(`https://pokeapi.co/api/v2/type/`)
        .then(el => el.json())
        .then(el => {
          this.types = el.results
        })

      await fetch(`https://pokeapi.co/api/v2/ability/?limit=300`)
        .then(el => el.json())
        .then(el => {
          this.abilities = el.results
        })
    }
  },
  mounted() {
    this.startVars()
    document.title = 'PokeApp'
  }
}
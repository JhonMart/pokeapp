import List from '@/components/list/List'
import Search from '@/components/search/Search'
import Pagination from '@/components/pagination/Pagination'
import Modal from '@/components/modal/Modal'
import Toast from '@/components/toast/Toast'
import { mapGetters } from 'vuex'

export default {
  name: 'Home',
  components: {
    List,
    Search,
    Modal,
    Pagination,
    Toast
  },
  computed: {
    ...mapGetters(["modalState"])
  }
}
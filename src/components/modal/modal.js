import Tabs from '@/components/detail/Tabs.vue'
import Types from '@/components/detail/Types.vue'
import { mapMutations, mapGetters } from 'vuex'

export default {
  name: 'Modal',
  components: {
    Tabs,
    Types
  },
  methods: {
    ...mapMutations([
      "setModal",
      "selectPokemon"
    ]),
    close() {
      this.setModal(false)
    },
    imgChangeOver() {
      setTimeout(() => {
        let pokemon = this.pokemon_selected,
          actualImg = pokemon.img

        if (actualImg && actualImg.includes('animated') && !actualImg.includes('/back/')) {
          actualImg = actualImg.replace(/([\D]+)([\d\D]+)/g, '$1back/$2')

          this.selectPokemon({
            ...pokemon,
            img: actualImg
          })
        }
      }, 100)
    },
    imgChangeOut() {
      setTimeout(() => {
        let pokemon = this.pokemon_selected,
          actualImg = pokemon.img

        if (actualImg && actualImg.includes('animated') && actualImg.includes('/back/')) {
          actualImg = actualImg.replace(/\/back/g, '')

          this.selectPokemon({
            ...pokemon,
            img: actualImg
          })
        }
      }, 100)
    },
    changeImgError() {
      let pokemon = this.pokemon_selected

      const firstLinkPNG = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${+pokemon.id}.png`,
        secondLinkPNG = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${+pokemon.id}.png`,
        failLoad = `https://www.clipartmax.com/png/full/129-1298464_open-pokeball-download-open-pokeball.png`

      if (pokemon.img != firstLinkPNG && pokemon.img != secondLinkPNG && pokemon.img != failLoad) {
        pokemon.title = 'Sprite SVG not found, load PNG'
        pokemon.alt = 'SVG not found image'
        pokemon.img = firstLinkPNG
      } else if (pokemon.img != secondLinkPNG && pokemon.img != failLoad) {
        pokemon.title = 'Sprite SVG not found, load PNG'
        pokemon.alt = 'SVG not found image'
        pokemon.img = secondLinkPNG
      } else if (pokemon.img == secondLinkPNG) {
        pokemon.title = 'Sprite not found'
        pokemon.alt = 'Not found image'
        pokemon.img = failLoad
      }

      this.selectPokemon(pokemon)
    },

  },
  computed: {
    ...mapGetters([
      "pokemon_selected",
      "modalState"
    ]),
    padSelectedPokemon() {
      let pokemon = this.pokemon_selected

      return pokemon && {
        ...pokemon,
        name: pokemon.name && pokemon.name.replace(/[_-]/g, ' ')
      }
    }
  },
}
import { mapMutations, mapGetters } from "vuex"

export default {
  name: 'Toast',
  computed: {
    ...mapGetters(["toast_list"]),
    listAlerts: function() {
      let list = this.toast_list

      return list
    }
  },
  methods: {
    ...mapMutations(["setAllToast"]),
    verifyToast() {
      let list = this.toast_list,
        active = list.filter(toast => (toast.moment + 3000) > Date.now())
        .map(toast => ({
          ...toast,
          out: (toast.moment + 2000) >= Date.now()
        }))

      this.setAllToast(active)
    }
  },
  mounted() {
    this.verifyToast()
  },
}
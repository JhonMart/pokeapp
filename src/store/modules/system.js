const getDefaultState = () => {
  return {
    modalState: false,
    toast_list: [],

    currentPage: 1,
    totalPages: 0
  };
};

const state = getDefaultState();

const mutations = {
  setNewToast(state, text) {
    const toast_obj = {
      text,
      moment: Date.now(),
      out: false
    }

    state.toast_list = [...state.toast_list, toast_obj]
  },
  setAllToast(state, all) {
    state.toast_list = all
  },
  setModal(state, stateModal) {
    let body = document.querySelector('body')
    state.modalState = stateModal

    if (stateModal) {
      document.querySelector('.tabs a') && document.querySelector('.tabs a').click()
      body.style.overflow = 'hidden'
      setTimeout(() => {
        if (document.querySelector('#modal-detail')) document.querySelector('#modal-detail').scrollTop = 0
      })
    } else {
      body.style.overflow = 'auto'
    }
  },

  setCurrentPage(state, currentPage) {
    state.currentPage = currentPage
  },
  setTotalPages(state, totalPages) {
    state.totalPages = totalPages
  }
};

const actions = {
  
};

const getters = {
  modalState: state => state.modalState,
  toast_list: state => state.toast_list,

  currentPage: state => state.currentPage,
  totalPages: state => state.totalPages
};

export default { state, mutations, actions, getters };

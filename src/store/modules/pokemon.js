const getDefaultState = () => {
    return {
      typeSearch: 'https://pokeapi.co/api/v2/pokemon/',
      local_list_page_pokemon: [],
      list_page_pokemon: [],
      pokemon_selected: {},
      abilities: [],
      types: [],
      start: 0,
      qtdView: 24
    };
  };
  
  const state = getDefaultState();
  
  const mutations = {
    setTypeSearch(state, typeSearch) {
      state.typeSearch = typeSearch
    },
    setLocalPagePokemon(state, payload) {
      state.local_list_page_pokemon = payload
    },
    setPagePokemon(state, payload) {
      state.list_page_pokemon = payload
    },
    selectPokemon(state, payload) {
      state.pokemon_selected = payload
    },
    setStart(state, start) {
      state.start = start
    },
    setQtdView(state, qtdView) {
      state.qtdView = qtdView
    }
  };
  
  const actions = {
    search({ commit, state }) {
      commit('setPagePokemon', [])
      commit('setLocalPagePokemon', [])
  
      return new Promise((result) => {
        let start = state.start
  
        fetch(`${state.typeSearch}?offset=${start}&limit=${state.qtdView}`)
          .then(el => {
            if (el.status === 404) {
              return {
                id: 404,
                name: 'Luxio',
                abilities: 1
              }
            } else return el.json()
          })
          .then(el => {
            let listItems = (el.results || el.pokemon)
            if (el.abilities && !listItems) {
              commit('setPagePokemon', [{
                id: el.id,
                name: el.name,
                url: 'https://pokeapi.co/api/v2/pokemon/' + el.id,
                img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${el.id}.svg`
              }])
  
              commit('setLocalPagePokemon', [])
              commit('setTotalPages', 0)
            } else {
              let sampleList = listItems.map(camps => ({
                url: camps.url || camps.pokemon.url,
                name: camps.pokemon ? camps.pokemon.name : camps.name,
                id: (camps.url || camps.pokemon.url).split('/')[6],
                img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${(camps.url || camps.pokemon.url).split('/')[6]}.svg`
              })) || []
  
              if (sampleList.length > state.qtdView) {
                commit('setCurrentPage', 1)
                commit('setPagePokemon', sampleList.slice(0, state.qtdView))
                commit('setLocalPagePokemon', sampleList)
                commit('setTotalPages', Math.ceil(sampleList.length / state.qtdView))
              } else {
                commit('setPagePokemon', sampleList)
                commit('setLocalPagePokemon', [])
                commit('setTotalPages', Math.ceil(el.count / state.qtdView))
              }
  
              if (state.start == 0)
                commit('setCurrentPage', 1)
            }
          })
          .catch(err => {
            console.log(err)
            alert('There was an unexpected error')
          })
          .finally(() => {
            result({})
          })
      })
    }
  };
  
  const getters = {
    typeSearch: state => state.typeSearch,
    local_list_page_pokemon: state => state.local_list_page_pokemon,
    list_page_pokemon: state => state.list_page_pokemon,
    pokemon_selected: state => state.pokemon_selected,
    start: state => state.start,
    qtdView: state => state.qtdView
  };
  
  export default { state, mutations, actions, getters };
  
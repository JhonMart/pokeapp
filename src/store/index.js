import Vue from 'vue'
import Vuex from 'vuex'

import system from "./modules/system";
import pokemon from "./modules/pokemon";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    pokemon,
    system
  }
})
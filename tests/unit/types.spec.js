import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Types from '@/components/detail/Types.vue'

describe('Types', () => {
  it('Count types', () => {
    const types = [
      { type: { name: 'normal' } },
      { type: { name: 'grass' } },
    ]

    const wrapper = shallowMount(Types, {
      propsData: { types }
    })

    setTimeout(()=>{
      expect(wrapper.findAll('.types .chip').length).equal(types.length)
    }, 500)
  })

  it('Check value', () => {
    const types = [
      { type: { name: 'fire' } },
    ]

    const wrapper = shallowMount(Types, {
      propsData: { types }
    })

    expect(wrapper.find('.types .chip').text()).equal(types[0].type.name)
  })

  it('Check personColor', () => {
    const wrapper = shallowMount(Types, {
      data() {
        return {
          colors: {
            "normal": ['grey', 'grey'],
            "fighting": ['#bcbce0', 'grey']
          }
        }
      }
    })

    setTimeout(()=>{
      expect(wrapper.vm.personColor('fighting'))
        .equal('background: linear-gradient(#bcbce0, grey)')
    })
  })

})
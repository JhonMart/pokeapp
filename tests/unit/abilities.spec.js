import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Abilities from '@/components/detail/Abilities.vue'

describe('Abilities', () => {
  it('Count ability', () => {
    const abilities = [
      { ability: { name: 'solar-power' } },
      { ability: { name: 'blazer' } },
      { ability: { name: 'torrent' } },
    ]

    const wrapper = shallowMount(Abilities, {
      propsData: { abilities }
    })

    setTimeout(() => {
      expect(wrapper.findAll('.abilities .chip').length).equal(abilities.length)
    }, 500)
  })

})
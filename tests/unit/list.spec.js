import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import List from '@/components/list/List.vue'
import store from '@/store'

const arrayPokemon = [{
    name: 'raticate',
    id: '20',
    img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg'
  },
  {
    name: 'caterpie',
    id: '10',
    img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg'
  },
  {
    name: 'garchomp-mega',
    id: '10058',
    img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10058.svg'
  },
  {
    name: 'rattata-alola',
    id: '10091',
    img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10091.svg'
  }
]
store.commit('setPagePokemon', arrayPokemon)


const wrapper = mount(List, {
  sync: false,
})

describe('List', () => {

  it('Count cards', () => {
    setTimeout(() => {
      expect(wrapper.findAll('aside .card').length).equal(arrayPokemon.length)
    }, 500)
  })

  it('Check card name', () => {
    setTimeout(() => {
      arrayPokemon.map((pk, id) => {
        expect(wrapper.findAll('.title').at(id).text()).equal(pk.name)
      })
    }, 500)
  })

  it('Check card id', () => {
    setTimeout(() => {
      arrayPokemon.map((pk, id) => {
        expect(wrapper.findAll('.id').at(id).text()).equal('#' + pk.id)
      })
    }, 500)
  })

  it('Check image default', () => {
    setTimeout(() => {
      expect(wrapper.find('img[src*=svg]').attributes('alt')).equal('')
    }, 500)
  })

  it('Check load PNG error', () => {
    setTimeout(() => {
      expect(wrapper.find('img[src*=png]').attributes('alt')).equal('SVG not found image')
    }, 500)
  })

  it('Check load SVG error', () => {
    setTimeout(() => {
      expect(wrapper.find('img[alt*=Not]').attributes('alt')).equal('Not found image')
    }, 500)
  })

  it('Detail open modal', () => {
    const id = 255

    wrapper.vm.detail(id)
      .then(() => {
        setTimeout(() => {
          expect(store.state.pokemon_selected.name).equal('torchic')
          expect(store.state.pokemon_selected.id).equal(id)
        }, 5000)
      })
  })

})
import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Sprites from '@/components/detail/Sprites.vue'

describe('Sprites', () => {
  it('Count valid sprites', () => {
    const sprites = {
      back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/3.png",
      back_female: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/female/3.png",
      back_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/3.png",
    }

    const wrapper = shallowMount(Sprites, {
      propsData: { sprites }
    })

    setTimeout(()=>{
      expect(wrapper.findAll('.sprites .card-image').length).equal(Object.keys(sprites).length)
    }, 500)
  })

  it('Invalid sprites', () => {
    const sprites = {
      back_default: "",
      back_female: undefined,
      back_shiny: null,
    }

    const wrapper = shallowMount(Sprites, {
      propsData: { sprites }
    })

    expect(wrapper.findAll('.sprites .card-image').length).equal(0)
  })

  it('Check value', () => {
    const sprites = {
      back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/3.png",
      back_female: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/female/3.png",
      back_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/3.png",
    }

    const wrapper = shallowMount(Sprites, {
      propsData: { sprites }
    })

    expect(wrapper.find('.sprites img').attributes('src')).equal(sprites[Object.keys(sprites)[0]])
  })

})
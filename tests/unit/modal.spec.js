import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Modal from '@/components/modal/Modal.vue'

import store from '@/store'

const pokemon = {
  name: 'raticate',
  id: '20',
  img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg'
}

store.commit('setPagePokemon', pokemon)

const wrapper = shallowMount(Modal, {
  propsData: { pokemon, open: true }
})

describe('Modal', () => {
  it('Check name', () => {
    setTimeout(() => {
      expect(wrapper.find('.name-pokemon').text()).equal(pokemon.name)
    }, 500)
  })

  it('Check id', () => {
    setTimeout(() => {
      expect(wrapper.find('.info.chip').text()).equal('#' + pokemon.id)
    }, 500)
  })

  it('Check img', () => {
    setTimeout(() => {
      expect(wrapper.find('img[src*=svg]').attributes('src')).equal(pokemon.img)
    }, 500)
  })

  it('Check open', () => {
    wrapper.vm.close()

    setTimeout(() => {
      expect(wrapper.find('#modal-detail').attributes('class')).equal('modal transparent')
      expect(wrapper.find('.modal-overlay').attributes('class')).equal('modal-overlay')
    }, 500)
  })

})
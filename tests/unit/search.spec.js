import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Search from '@/components/search/Search.vue'
import store from '@/store'

const arrayPokemon = [{
      name: 'raticate',
      id: '20',
      img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg'
    },
    {
      name: 'caterpie',
      id: '10',
      img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg'
    },
    {
      name: 'garchomp-mega',
      id: '10058',
      img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10058.svg'
    },
    {
      name: 'rattata-alola',
      id: '10091',
      img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10091.svg'
    }
  ],
  paramObj = {
    types: [
      { name: 'fire', url: 'https://pokeapi.co/api/v2/type/10/' },
      { name: 'water', url: 'https://pokeapi.co/api/v2/type/11/' }
    ],
    abilities: [
      { name: 'overgrow', url: 'https://pokeapi.co/api/v2/ability/65/' },
      { name: 'chlorophyll', url: 'https://pokeapi.co/api/v2/ability/34/' },
      { name: 'blaze', url: 'https://pokeapi.co/api/v2/ability/66/' }
    ],
  }

const qtd = 2
const wrapper = shallowMount(Search)

describe('Search', () => {
  it('Start value input', () => {
    const val = ''

    expect(wrapper.find('input[type="text"]').attributes('value')).equal(val)
  })

  it('New value input', () => {
    const val = 'fire'

    wrapper.find('input[type="text"]').setValue(val)

    expect(wrapper.vm.keyword).equal(val)
  })

  it('Check exists all functions', () => {
    expect(wrapper.vm.pressKey).to.be.a("function")
    expect(wrapper.vm.search).to.be.a("function")
  })

  it('Check mount types', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {
          types: [{ name: 'fire' }, { name: 'water' }],
        }
      }
    })

    expect(wrapper.vm.types).length(2)
  })

  it('Check mount abilitys', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {
          abilitys: [{ name: 'overgrow' }, { name: 'chlorophyll' }, { name: 'blaze' }],
        }
      }
    })

    expect(wrapper.vm.abilitys).length(3)
  })

  it('Check create url: name', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {...paramObj }
      }
    })

    wrapper.vm.keyword = 'Charizard'
    wrapper.vm.pressKey({ keyCode: 13 })

    expect(wrapper.vm.searchURL).equal('https://pokeapi.co/api/v2/pokemon/charizard')
  })

  it('Check create url: type', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {...paramObj }
      }
    })

    wrapper.vm.keyword = 'water'
    wrapper.vm.pressKey({ keyCode: 13 })

    expect(wrapper.vm.searchURL).equal('https://pokeapi.co/api/v2/type/11/')
  })

  it('Check create url: ability', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {...paramObj }
      }
    })

    wrapper.vm.keyword = paramObj.abilities[0].name
    wrapper.vm.pressKey({ keyCode: 13 })

    expect(wrapper.vm.searchURL).equal(paramObj.abilities[0].url)
  })

  it('Check create url: id', () => {
    const wrapper = shallowMount(Search, {
      data() {
        return {...paramObj }
      }
    })

    wrapper.vm.keyword = '06'
    wrapper.vm.pressKey({ keyCode: 13 })

    expect(wrapper.vm.searchURL).equal('https://pokeapi.co/api/v2/pokemon/6')
  })

  it('Check vars', () => {
    setTimeout(() => {
      const wrapper = shallowMount(Search)

      return wrapper.vm.startVars()
        .then(() => {
          expect(wrapper.vm.types[0]).haveOwnProperty('name')
          expect(wrapper.vm.abilitys[0]).haveOwnProperty('name')
        })
    }, 500)

  })

  it('Normal pagination', () => {
    store.commit('setQtdView', qtd)
    store.commit('setPagePokemon', arrayPokemon)

    setTimeout(() => {
      expect(store.state.list_page_pokemon.length).equal(qtd)
    }, 500)
  })

  it('Type pagination', () => {
    store.commit('setQtdView', qtd)
    store.commit('setPagePokemon', arrayPokemon)
    store.commit('setTypeSearch', 'https://pokeapi.co/api/v2/type/8/')

    setTimeout(() => {
      expect(store.state.list_page_pokemon.length).equal(qtd)
      expect(store.state.local_list_page_pokemon.length).equal(4)
    }, 500)
  })

  it('Ability pagination', () => {
    store.commit('setQtdView', qtd)
    store.commit('setPagePokemon', arrayPokemon)
    store.commit('setTypeSearch', 'https://pokeapi.co/api/v2/ability/66/')

    setTimeout(() => {
      expect(store.state.list_page_pokemon.length).equal(qtd)
      expect(store.state.local_list_page_pokemon.length).equal(4)
    }, 500)
  })

})
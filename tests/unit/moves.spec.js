import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Moves from '@/components/detail/Moves.vue'

describe('Moves', () => {
  it('Count moves', () => {
    const moves = [
      { move: { name: 'cut' } },
      { move: { name: 'swords-dance' } },
    ]

    const wrapper = shallowMount(Moves, {
      propsData: { moves }
    })

    expect(wrapper.findAll('.moves .collection-item').length).equal(moves.length)
  })

  it('Check value', () => {
    const moves = [
      { move: { name: 'cut' } },
      { move: { name: 'swords-dance' } },
    ]

    const wrapper = shallowMount(Moves, {
      propsData: { moves }
    })

    expect(wrapper.find('.moves .collection-item').text()).equal(moves[0].move.name)
  })

})
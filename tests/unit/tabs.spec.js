import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import Tabs from '@/components/detail/Tabs.vue'

import Abilities from '@/components/detail/Abilities.vue'
import Moves from '@/components/detail/Moves.vue'
import Sprites from '@/components/detail/Sprites.vue'
import Stats from '@/components/detail/Stats.vue'

const pokemon = {
  name: 'blastoise',
  id: '3',
  abilities: [
    { ability: { name: 'solar-power' } },
    { ability: { name: 'blazer' } },
    { ability: { name: 'torrent' } },
  ],
  moves: [
    { move: { name: 'cut' } },
    { move: { name: 'swords-dance' } },
  ],
  sprites: {
    back_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/3.png",
    back_female: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/female/3.png",
    back_shiny: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/3.png",
  },
  stats: [
    { stat: { name: 'hp' }, base_stat: 42 },
    { stat: { name: 'defense' }, base_stat: 66 },
  ],
  types: [
    { type: { name: 'normal' } },
    { type: { name: 'grass' } },
  ],
}

const wrapper = mount(Tabs, {
  components: {
    Stats,
    Moves,
    Sprites,
    Abilities
  },
  propsData: { pokemon }
})

describe('Tabs', () => {
  it('Count tabs', () => {
    setTimeout(()=>{
      expect(wrapper.findAll('.tabs li').length).equal(4)
    }, 500)
  })

  it('Check change tab', () => {
    wrapper.find('.tabs li:nth-child(2)').trigger('click')

    expect(wrapper.find('#move').isVisible()).equal(true)
  })

  it('Check ability', () => {
    setTimeout(()=>{
      expect(wrapper.findAll('.abilities .chip').length).equal(pokemon.abilities.length)
    }, 500)
  })

  it('Check moves', () => {
    expect(wrapper.findAll('.moves .collection-item').length).equal(pokemon.moves.length)
  })

  it('Check sprites', () => {
    expect(wrapper.findAll('.sprites .card-image').length).equal(Object.keys(pokemon.sprites).length)
  })

  it('Check stats', () => {
    expect(wrapper.findAll('.stats .progress').length).equal(pokemon.stats.length)
  })

})
import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Stats from '@/components/detail/Stats.vue'

describe('Stats', () => {
  it('Count stats', () => {
    const stats = [
      { stat: { name: 'hp' }, base_stat: 42 },
      { stat: { name: 'defense' }, base_stat: 66 },
    ]

    const wrapper = shallowMount(Stats, {
      propsData: { stats }
    })

    expect(wrapper.findAll('.stats .progress').length).equal(stats.length)
  })

  it('Check name', () => {
    const stats = [
      { stat: { name: 'hp' }, base_stat: 42 },
      { stat: { name: 'defense' }, base_stat: 66 },
    ]

    const wrapper = shallowMount(Stats, {
      propsData: { stats }
    })

    setTimeout(() => {
      expect(wrapper.find('.stats label').text()).equal(stats[0].stat.name)
    }, 500)
  })

  it('Check value', () => {
    const stats = [
      { stat: { name: 'hp' }, base_stat: 42 },
      { stat: { name: 'defense' }, base_stat: 66 },
    ]

    const wrapper = shallowMount(Stats, {
      propsData: { stats }
    })

    setTimeout(() => {
      expect(wrapper.find('.stats .progress')
          .attributes('title'))
        .equal('Points: ' + stats[0].base_stat)
    }, 1000)
  })

  it('Check color', () => {
    const stats = [
      { stat: { name: 'hp' }, base_stat: 42 },
      { stat: { name: 'defense' }, base_stat: 66 },
    ]

    const wrapper = shallowMount(Stats, {
      propsData: { stats }
    })

    expect(wrapper.find('.stats .progress div').attributes('class')).equal('determinate teal lighten-2')
  })

})